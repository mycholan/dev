<?php 

class bcardHandler {
    
    /**
     *
     * ID of the business card product
     *
     */
    private $bcard_pid = 68;
    
    /**
     *
     * Silver card holder price
     *
     */
    private $card_holder_silver = 100;
    
    /**
     *
     * Black card holder price
     *
     */
    private $card_holder_block = 110;
    
    /**
     *
     * Additional art work cost
     *
     */
    private $printable_final_image = 250;
    
    /**
     *
     * Business card material config
     * Used to to determine single card price
     *
     */
    private $dcard_prices = array(
        "format" => array(
            "9x54" => .10,
            "7x45" => .7
        ),
        "side_of_print" => array(
            "front" => .15,
            "both" => .25,
        ),
        "paper_type" => array(
            "260g" => .10,
            "310g" => .12,
            "350g" =>  .15
        ),
        "finishing" => array(
            "none" => 0,
            "matte" => .20,
            "glossy" => .15
        ),
        "rounded_corner" => array(
            "none" => 0,
            "one_corner" => .5,
            "two_corner" => .10,
            "three_corner" => .15,
            "all_corner" => .20,
        ),
        "card_holder" => array(
            "none" => 0,
            "black" => 10,
            "silver" => 12,
        )
    );
    
    /**
     *
     * Pricing slot array
     * Used to determine the pricing of business card
     *
     */
    private $quantity = array(
        "100" => 5,
        "200" => 8,
        "300" => 10,
        "400" => 12,
        "500" => 14,
        "1000" => 16,
        "1500" => 18,
        "2000" => 20,
        "2500" => 22,
        "3000" => 25
    );
    
    public function __construct() {
        
        add_filter('woocommerce_add_cart_item',  array($this, 'bcard_pricing_handler'), 999, 2);
        add_filter('woocommerce_get_cart_item_from_session',  array($this, 'bcard_pricing_handler'), 999, 2);
        
        add_filter('woocommerce_get_item_data', array($this, 'bcard_custom_meta_handler'), 999, 2);
        
        /* WC 3.0.6 update */
        if (version_compare(WC()->version, '3.0.0', '<')) {
            add_action('woocommerce_add_order_item_meta', array($this, 'bcard_order_handler'), 99, 3);
        } else {
            add_action('woocommerce_new_order_item', array($this, 'bcard_order_handler'), 99, 3);
        }
        
        add_action('wp_ajax_nopriv_bcard_get_pricing_slots', array($this, 'bcard_determine_price'));
        add_action('wp_ajax_bcard_get_pricing_slots', array($this, 'bcard_determine_price'));
        
        add_action('wp_ajax_nopriv_process_bcard_design', array($this, 'process_bcard_design'));
        add_action('wp_ajax_process_bcard_design', array($this, 'process_bcard_design'));
                
    }
    
    function bcard_custom_meta_handler($_cart_data, $_cart_item = null) {
        
        $custom_fields = array();
        
        /* Woo 2.4.2 updates */
        if (! empty($_cart_data)) {
            $custom_fields = $_cart_data;       
        }
        
        if (isset($_cart_item["format"])) {            
            $custom_fields[] = array("name" => "Format", "value" => $_cart_item["format"]);            
        }
        if (isset($_cart_item["side_of_print"])) {
            $custom_fields[] = array("name" => "Side of Print", "value" => $_cart_item["side_of_print"]);
        }
        if (isset($_cart_item["paper_type"])) {
            $custom_fields[] = array("name" => "Paper Type", "value" => $_cart_item["paper_type"]);
        }
        if (isset($_cart_item["finishing"])) {
            $custom_fields[] = array("name" => "Finishing", "value" => $_cart_item["finishing"]);
        }
        if (isset($_cart_item["rounded_corner"])) {
            $custom_fields[] = array("name" => "Rounded Corner", "value" => $_cart_item["rounded_corner"]);
        }
        if (isset($_cart_item["card_holder"])) {
            $custom_fields[] = array("name" => "Card Holder", "value" => $_cart_item["card_holder"]);
        }
        
        if (isset($_cart_item["bcard_design_upload"])) {
            $custom_fields[] = array("name" => "User Design", "value" => $_cart_item["bcard_design_upload"]);
        } else {
            /* User details */
            if (isset($_cart_item["user_name"])) {
                $custom_fields[] = array("name" => "User Name", "value" => $_cart_item["user_name"]);
            }
            if (isset($_cart_item["user_mobile"])) {
                $custom_fields[] = array("name" => "User Mobile", "value" => $_cart_item["user_mobile"]);
            }
            if (isset($_cart_item["user_email"])) {
                $custom_fields[] = array("name" => "User Email", "value" => $_cart_item["user_email"]);
            }
            if (isset($_cart_item["user_company"])) {
                $custom_fields[] = array("name" => "User Company", "value" => $_cart_item["user_company"]);                
            }
            if (isset($_cart_item["user_designation"])) {
                $custom_fields[] = array("name" => "User Designation", "value" => $_cart_item["user_designation"]);                
            }
        }
        
        return $custom_fields;
        
    }
   
    /**
     *
     * Called when the order is being placed
     * Used to inject custom order meta to the order line item
     *
     */
    function bcard_order_handler($_item_id, $_values, $_cart_item_key) {
                
        /* WC 3+ & Older versions - compatible */
        if (!version_compare(WC()->version, '3.0.0', '<') && isset($_values->legacy_values)) {
            $_values = $_values->legacy_values;
        }

        if (isset($_values["format"])) {
            wc_add_order_item_meta($_item_id, "Format", $_values["format"]);
        }
        if (isset($_values["side_of_print"])) {
            wc_add_order_item_meta($_item_id, "Side of Print", $_values["side_of_print"]);
        }
        if (isset($_values["paper_type"])) {
            wc_add_order_item_meta($_item_id, "Paper Type", $_values["paper_type"]);
        }
        if (isset($_values["finishing"])) {
            wc_add_order_item_meta($_item_id, "Finishing", $_values["finishing"]);
        }
        if (isset($_values["rounded_corner"])) {
            wc_add_order_item_meta($_item_id, "Rounded Corner", $_values["rounded_corner"]);
        }
        if (isset($_values["card_holder"])) {
            wc_add_order_item_meta($_item_id, "Card Holder", $_values["card_holder"]);
        }
        
        if (isset($_values["bcard_design_upload"])) {            
            /* User design upload */
            wc_add_order_item_meta($_item_id, "User Design", $_values["bcard_design_upload"]);            
        } else {            
            /* User details */            
            if (isset($_values["user_name"])) {                
                wc_add_order_item_meta($_item_id, "User Name", $_values["user_name"]);     
            }
            if (isset($_values["user_mobile"])) {                
                wc_add_order_item_meta($_item_id, "User Mobile", $_values["user_mobile"]);     
            }
            if (isset($_values["user_email"])) {                
                wc_add_order_item_meta($_item_id, "User Email", $_values["user_email"]);     
            }
            if (isset($_values["user_company"])) {                
                wc_add_order_item_meta($_item_id, "User Company", $_values["user_company"]);     
            }
            if (isset($_values["user_designation"])) {                
                wc_add_order_item_meta($_item_id, "User Designation", $_values["user_designation"]);     
            }            
        }
        
        wc_add_order_item_meta($_item_id, "", $_values[""]);
        
    }
    
    /**
     *
     * Called when the product is being added to the card
     * THis handler will calculate the price and update the cart item
     *
     */
    function bcard_pricing_handler($_citem, $_cart_item_key) {
        
        if ($_citem["product_id"] == $this->bcard_pid && isset($_citem["qty"])) {
            
            $qty = intVal($_citem["qty"]);
            $discount = $this->quantity[$_citem["qty"]];
            $price = $this->determine_single_bcard_price($_citem);
            $totalPrice = $qty * $price;
            
            $finalPrice = ($totalPrice - ($totalPrice * $discount / 100));            
            
            /* Update the price */
            if (method_exists ($_citem ["data"], "set_price")) {
                /* Woocommerce 3.0.6 + */
                $_citem["data"]->set_price($finalPrice);
            } else {
                /* Woocommerece before 3.0.6 */
                $_citem["data"]->price = $finalPrice;
            }
        }        
        
        return $_citem;
        
    }
    
    /**
     *
     * Ajax handler for calculating business card price
     *
     */
    public function bcard_determine_price() {      
        
        $payload = json_decode(str_replace('\"','"',$_REQUEST["bcard_param"]), true);
        
        $price = $this->determine_single_bcard_price($payload);        
        
        /* Now we have exact price for a single card */        
        $prices = array();
        
        foreach ($this->quantity as $qty => $discount) { 
            
            if (isset($payload["card_holder"])) {
                if ($payload["card_holder"] == "black") {
                    
                } else if($payload["card_holder"] == "silver") {
                    
                }
            }
            
            $totalPrice = $price * intval($qty);
            $finalPrice = ($totalPrice - ($totalPrice * $discount / 100));
            
            $prices[$qty] = array(
                "price" => $finalPrice,                
                "single" => ($finalPrice / intval($qty))
            );
            
        }
        
        echo json_encode($prices);
        exit();
        
    }    
    
    private function determine_single_bcard_price($_payload) {
        
        $price = 0;
        
        if (isset($_payload["format"])) {
            /* Format price */
            $price += $this->dcard_prices["format"][$_payload["format"]];
        }        
        if (isset($_payload["side_of_print"])) {
            /* Sides of print price */
            $price += $this->dcard_prices["side_of_print"][$_payload["side_of_print"]];
        }        
        if (isset($_payload["paper_type"])) {
            /* Paper type price */
            $price += $this->dcard_prices["paper_type"][$_payload["paper_type"]];
        }        
        if (isset($_payload["finishing"])) {
            /* Finishing price */
            $price += $this->dcard_prices["finishing"][$_payload["finishing"]];
        }        
        if (isset($_payload["rounded_corner"])) {
            /* Rounded corner price */
            $price += $this->dcard_prices["rounded_corner"][$_payload["rounded_corner"]];
        }
        
        return $price;
        
    }
    
    public function process_bcard_design() {
        
        $payload = array();
       
        if (isset($_REQUEST["format"])) {
            $payload["format"] = $_REQUEST["format"];
        }
        if (isset($_REQUEST["side_of_print"])) {
            $payload["side_of_print"] = $_REQUEST["side_of_print"];
        }
        if (isset($_REQUEST["paper_type"])) {
            $payload["paper_type"] = $_REQUEST["paper_type"];
        }
        if (isset($_REQUEST["finishing"])) {
            $payload["finishing"] = $_REQUEST["finishing"];
        }
        if (isset($_REQUEST["rounded_corner"])) {
            $payload["rounded_corner"] = $_REQUEST["rounded_corner"];
        }
        if (isset($_REQUEST["card_holder"])) {
            $payload["card_holder"] = $_REQUEST["card_holder"];
        }
        
        if (isset($_REQUEST["qty"])) {
            $payload["qty"] = $_REQUEST["qty"];
        }
        
        if (isset($_FILES['bcard_design'])) {
            
            $res = $this->process_design_upload();
            $payload["bcard_design_upload"] = $res;
            
        } else {
            
            if (isset($_REQUEST["user_name"])) {
                $payload["user_name"] = $_REQUEST["user_name"];
            }
            if (isset($_REQUEST["user_mobile"])) {
                $payload["user_mobile"] = $_REQUEST["user_mobile"];
            }
            if (isset($_REQUEST["user_email"])) {
                $payload["user_email"] = $_REQUEST["user_email"];
            }
            if (isset($_REQUEST["user_company"])) {
                $payload["user_company"] = $_REQUEST["user_company"];
            }
            if (isset($_REQUEST["user_designation"])) {
                $payload["user_designation"] = $_REQUEST["user_designation"];
            }
            
        }        
        
        return WC()->cart->add_to_cart($this->bcard_pid, 1, 0, array(), $payload);
        
    }
    
    private function process_design_upload() {
        
        $res = "";
        if (! function_exists('wp_handle_upload')) {
            require_once (ABSPATH . 'wp-admin/includes/file.php');
        }
        
        if (isset($_FILES['bcard_design'])) {
            $res = wp_handle_upload($_FILES['bcard_design'], array(
                'test_form' => false
            ));
        }
        
        if (!isset($res['error'])) {
            $res = $res["url"];
        } else {
            $res = "";
        }
        
        return $res;
    }
    
}


new bcardHandler();


?>

