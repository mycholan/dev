<?php get_header(); ?>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/bcard-designer.css">

<script type="text/javascript">
	bcard_ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>

<h1><?php the_title(); ?></h1>
<p><?php the_content(); ?></p>

<table class="bcard-designer-table">
	<tr>
		<td>
			<div class="bcard-configuration-wrapper">
			
				<h1>Configure your Business Cards</h1>
			
				<div class="bcard-config-block">
					<h3><span>1</span> Format</h3>
					<div data-type="format" class="bcard-option-radio-wrapper format">
						<Label><input type="radio" name="format" data-label="9 X 5.4" value="9x54"/> 9 X 5.4</Label>						
					</div>
				</div>
				<div class="bcard-config-block collapse">
					<h3><span>2</span> Side of print</h3>
					<div data-type="side_of_print" class="bcard-option-radio-wrapper side_of_print">
						<Label><input type="radio" name="side_of_print" data-label="Full color on Front" value="front"/> Full color on Front</Label>
						<Label><input type="radio" name="side_of_print" data-label="Full color on Fron & Back" value="both"/>  Full color on Fron & Back</Label>
					</div>
				</div>
				<div class="bcard-config-block collapse">
					<h3><span>3</span> Paper Type</h3>
					<div data-type="paper_type" class="bcard-option-radio-wrapper paper_type">
						<Label><input type="radio" name="paper_type" data-label="260g Art Card" value="260g"/> 260g Art Card</Label>
						<Label><input type="radio" name="paper_type" data-label="310g Art Card" value="310g"/> 310g Art Card</Label>
						<Label><input type="radio" name="paper_type" data-label="350g Art Card" value="350g"/>  350g Art Card</Label>
					</div>
				</div>
				<div class="bcard-config-block collapse">
					<h3><span>4</span> Finishing</h3>
					<div data-type="finishing" class="bcard-option-radio-wrapper finishing">
						<Label><input type="radio" name="finishing" data-label="None" value="none"/> None</Label>
						<Label><input type="radio" name="finishing" data-label="Matte Lamination" value="matte"/>  Matte Lamination</Label>
						<Label><input type="radio" name="finishing" data-label="Glossy Lamination" value="glossy"/>  Glossy Lamination</Label>
					</div>
				</div>
				<div class="bcard-config-block collapse">
					<h3><span>5</span> Rounded Corners</h3>
					<div data-type="rounded_corner" class="bcard-option-radio-wrapper rounded_corner">
						<Label><input type="radio" name="rounded-corner" data-label="None" value="none"/> None</Label>
						<Label><input type="radio" name="rounded-corner" data-label="One Rounded Corner" value="one_corner"/> One Rounded Corner</Label>
						<Label><input type="radio" name="rounded-corner" data-label="Two Rounded Corner" value="two_corner"/> Two Rounded Corner</Label>
						<Label><input type="radio" name="rounded-corner" data-label="Three Rounded Corner" value="three_corner"/> Three Rounded Corner</Label>
						<Label><input type="radio" name="rounded-corner" data-label="Four Rounded Corner" value="four_corner"/> Four Rounded Corner</Label>
						
						<div class="bcard-round-corner-config" id="bcard-round-corner-none">
							<Label><input type="radio" name="bcard-round-corner-one-corner" data-label="No Rounded Corner" value="none"/> No Rounded Corner</Label>
						</div>
						<div class="bcard-round-corner-config" id="bcard-round-corner-one_corner">
							<Label><input type="radio" name="bcard-round-corner-one-corner" data-label="Top Left" value="top-left"/> Top Left</Label>
							<Label><input type="radio" name="bcard-round-corner-one-corner" data-label="Top Right" value="top-right"/> Top Right</Label>
							<Label><input type="radio" name="bcard-round-corner-one-corner" data-label="Bottom Right" value="bottom-right"/> Bottom Right</Label>
							<Label><input type="radio" name="bcard-round-corner-one-corner" data-label="Bottom Left" value="bottom-left"/> Bottom Left</Label>
						</div>
						<div class="bcard-round-corner-config" id="bcard-round-corner-two_corner">
							<Label><input type="radio" name="bcard-round-corner-two-corner" data-label="Top Corner" value="top"/> Top Corner</Label>
							<Label><input type="radio" name="bcard-round-corner-two-corner" data-label="Bottom Corner" value="bottom"/> Bottom Corner</Label>
							<Label><input type="radio" name="bcard-round-corner-two-corner" data-label="Left Corner" value="left"/> Left Corner</Label>
							<Label><input type="radio" name="bcard-round-corner-two-corner" data-label="Right Corner" value="right"/> Right Corner</Label>							
							<Label><input type="radio" name="bcard-round-corner-two-corner" data-label="Top-Left Bottom-Right Corner" value="top-left-bottom-right"/> Top-Left Bottom-Right Corner</Label>
							<Label><input type="radio" name="bcard-round-corner-two-corner" data-label="Top-Right Bottom-Left Corner" value="top-right-bottom-left"/> Top-Right Bottom-Left Corner</Label>
						</div>
						<div class="bcard-round-corner-config" id="bcard-round-corner-three_corner">
							<Label><input type="radio" name="bcard-round-corner-three-corner" data-label="Top Left and Bottom Corner" value="top-left-bottom"/> Top Left and Bottom Corner</Label>
							<Label><input type="radio" name="bcard-round-corner-three-corner" data-label="Top Right and Bottom Corner" value="top-right-bottom"/> Top Right and Bottom Corner</Label>
							<Label><input type="radio" name="bcard-round-corner-three-corner" data-label="Bottom Left and Top Corner" value="bottom-left-top"/> Bottom Left and Top Corner</Label>
							<Label><input type="radio" name="bcard-round-corner-three-corner" data-label="Bottom Right and Top Corner" value="bottom-right-top"/> Bottom Right and Top Corner</Label>
						</div>
						<div class="bcard-round-corner-config" id="bcard-round-corner-four_corner">
							<Label><input type="radio" name="bcard-round-corner-four-corner" data-label="All Rounded Corner" value="all"/> All Rounded Corner</Label>							
						</div>
						
					</div>
				</div>
				<div class="bcard-config-block collapse">
					<h3><span>6</span> Card Holder</h3>
					<div data-type="card_holder" class="bcard-option-radio-wrapper card_holder">
						<Label><input type="radio" name="card_holder" data-label="None" value="none"/> None</Label>
						<Label><input type="radio" name="card_holder" data-label="Black Card Holder" value="black"/> Black Card Holder</Label>
						<Label><input type="radio" name="card_holder" data-label="Silver Card Holder" value="silver"/> Silver Card Holder</Label>
					</div>
				</div>
				<div class="bcard-config-block collapse">
					<h3><span>7</span> Delivery Location</h3>
					<div data-type="delivery_location" class="bcard-option-radio-wrapper delivery_location">
						<Label><input type="radio" name="delivery_location" data-label="Chennai India" value="chennai"/> Chennai India</Label>
						<Label><input type="radio" name="delivery_location" data-label="" value=""/> Add new Address</Label>
					</div>
				</div>
				<div class="bcard-config-block collapse">
					<h3><span>8</span> Select your quantity and delivery date</h3>
					<div data-type="quantity" class="bcard-option-radio-wrapper quantity"></div>
				</div>
				<div class="bcard-config-block collapse">
					<h3><span>9</span> Artwork options</h3>
					<div data-type="art_option" class="bcard-option-radio-wrapper art_option">
					
    					<div class="bcard-option-tab-header" id="bcard-config-user-tab">					
    						<a href="#bcard-config-user-tab-details" class="active" data-type="details">Card Details</a>
    						<a href="#bcard-config-user-tab-upload" data-type="upload">Upload Design</a>
    					</div>
    					<div class="bcard-option-tab-content">
    						<div id="bcard-config-user-tab-details" style="display: block;">
    						
    							<input type="text" id="bcard-option-meta-name" placeholder="Name"/>
    							<input type="text" id="bcard-option-meta-mobile" placeholder="Mobile"/>
    							<input type="text" id="bcard-option-meta-email" placeholder="Email"/>
    							<input type="text" id="bcard-option-meta-company" placeholder="Company"/>
    							<input type="text" id="bcard-option-meta-designation" placeholder="Designation"/>    							
    						
    						</div>
    						<div id="bcard-config-user-tab-upload">
    						
    							<label><input type="file" id="bcard-designer-file-input" /> Upload</label>
    						
    						</div>
    					</div>
					
					</div>
				</div>
				
				<div class=""></div>
				
				<div class="bcard-config-footer">
					<button id="bcard-place-order-btn">Place Order</button>
				</div>
							
			</div>
		</td>
		<td>
			<div class="bcard-config-summary">
				
				<h1>Summary</h1>
				<table class="bcard-config-summary-table">
					<tr>
						<td>Product</td>
						<td><span id="bcard-summary-product">Business Card</span></td>
					</tr>
					<tr>
						<td>Paper Type</td>
						<td><span id="bcard-summary-paper-type">-</span></td>
					</tr>
					<tr>
						<td>Format</td>
						<td><span id="bcard-summary-format">-</span></td>
					</tr>
					<tr>
						<td>Pages</td>
						<td><span id="bcard-summary-pages">-</span></td>
					</tr>
					<tr>
						<td>Colors</td>
						<td><span id="bcard-summary-colors">-</span></td>
					</tr>
					<tr>
						<td>Card Holder</td>
						<td><span id="bcard-summary-card-holder">-</span></td>
					</tr>
					<tr>
						<td>Finishing</td>
						<td><span id="bcard-summary-finishing">-</span></td>
					</tr>
					<tr>
						<td>Rounded Corners</td>
						<td><span id="bcard-summary-rounded-corners">-</span></td>
					</tr>
					<tr>
						<td>Delivery Location</td>
						<td><span id="bcard-summary-delivery-location">-</span></td>
					</tr>
					<tr>
						<td>Quantity</td>
						<td><span id="bcard-summary-quantity">-</span></td>
					</tr>
					<!-- 
					<tr>
						<td>Delivery Date</td>
						<td><span id="bcard-summary-delivery-date">-</span></td>
					</tr>
					 -->
					<tr>
						<td>Sub total</td>
						<td><span id="bcard-summary-sub-total">-</span></td>
					</tr>
					<tr>
						<td>Total</td>
						<td><span id="bcard-summary-total">-</span></td>
					</tr>
				</table>
				
			</div>
		</td>
	</tr>
</table>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/bcard-designer.js"></script>

<?php get_footer(); ?>