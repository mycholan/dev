
(function($) {
	
	var uploadedUrl = "";
	
	$(document).on("click", "div.bcard-option-tab-header > a", function() {
		
		
		$(this).parent().next().find("> div").hide();
		$(this).siblings().removeClass("active");
		$(this).addClass("active");
		$($(this).attr("href")).show();
		
	});
	
	$(document).on("click", "#bcard-place-order-btn", function(e) {	
		addBcardToCart();	
	});
	
	var fInput = document.getElementById("bcard-designer-file-input");	
	fInput.addEventListener("change", function (evt) {
		
		var formData = new FormData();	
      	file = this.files[0];         			
		formData.append('action', "bcard_design_upload");
		formData.append('bcard_design', file, file.name);
			
		if (formData) {
		 	$.ajax({
			   	url: bcard_ajax_url,
			   	type: "POST",						
			   	data: formData,
			   	processData: false,
			   	contentType: false,
			   	success: function (res) {
					console.log(res); 
					uploadedUrl = res;
			   	}
			});      		
    	}
	});	
	
	$(document).on("click", "div.bcard-round-corner-config", function(e) {
		e.stopPropagation();
	});
		
	$(document).on("click", "div.bcard-round-corner-config > label > input[type=radio]", function(e) {
		
		let wasChecked = $(this).attr('data-was-checked');
		
		if (wasChecked == 'true') {
	        this.checked = false;
	        $(this).attr('data-was-checked', this.checked);
	    }
	    else {
	        this.checked = true;
	        $(this).attr('data-was-checked', this.checked);
	    }

		if ($(this).is(":checked")) {
			$("#bcard-summary-rounded-corners").html($(this).attr("data-label"));
			$("div.bcard-option-radio-wrapper.card_holder").parent().removeClass("collapse");			
		} else {		
			$("#bcard-summary-rounded-corners").html("-");
			$("div.bcard-option-radio-wrapper.card_holder").parent().addClass("collapse");			
		}
		
		e.stopPropagation();
		
	});
	
	$(document).on("click", "div.bcard-option-radio-wrapper > label > input[type=radio]", function(e) {  console.log("bcard option radio wrapper clicked");
		
		let wasChecked = $(this).attr('data-was-checked');
		
		if (wasChecked == 'true') {
	        this.checked = false;
	        $(this).attr('data-was-checked', this.checked);
	    }
	    else {
	        this.checked = true;
	        $(this).attr('data-was-checked', this.checked);
	    }
		
		let configType = $(this).closest("div.bcard-option-radio-wrapper").attr("data-type"); 
		
		if (configType == "format") {
			
			resetConfigSection("side_of_print");
			resetConfigSection("paper_type");
			resetConfigSection("finishing");
			resetConfigSection("rounded_corner");
			resetConfigSection("card_holder");
			resetConfigSection("delivery_location");
			resetConfigSection("quantity");
			resetConfigSection("art_option");				
			
			
			
			if ($(this).is(":checked")) {
				$("#bcard-summary-format").html($(this).attr("data-label"));
				$("div.bcard-option-radio-wrapper.side_of_print").parent().removeClass("collapse");
			} else {
				$("#bcard-summary-format").html("-");
				$("div.bcard-option-radio-wrapper.side_of_print").parent().addClass("collapse");	
			}		
			
		} else if (configType == "side_of_print") {
			
			resetConfigSection("paper_type");
			resetConfigSection("finishing");
			resetConfigSection("rounded_corner");
			resetConfigSection("card_holder");
			resetConfigSection("delivery_location");
			resetConfigSection("quantity");
			resetConfigSection("art_option");
			
			if ($(this).is(":checked")) {				
				if ($(this).val() == "front") {
					$("#bcard-summary-pages").html("Front");
				} else {
					$("#bcard-summary-pages").html("Both");	
				}				
				$("#bcard-summary-colors").html($(this).attr("data-label"));				
				$("div.bcard-option-radio-wrapper.paper_type").parent().removeClass("collapse");
			} else {
				$("#bcard-summary-pages").html("-");
				$("#bcard-summary-colors").html("-");
				$("div.bcard-option-radio-wrapper.paper_type").parent().addClass("collapse");
			}
			
		} else if (configType == "paper_type") {
			
			resetConfigSection("finishing");
			resetConfigSection("rounded_corner");
			resetConfigSection("card_holder");
			resetConfigSection("delivery_location");
			resetConfigSection("quantity");
			resetConfigSection("art_option");
			
			if ($(this).is(":checked")) {
				$("#bcard-summary-paper-type").html($(this).attr("data-label"));
				$("div.bcard-option-radio-wrapper.finishing").parent().removeClass("collapse");
			} else {
				$("#bcard-summary-paper-type").html("-");
				$("div.bcard-option-radio-wrapper.finishing").parent().addClass("collapse");
			}
			
		} else if (configType == "finishing") {
			
			resetConfigSection("rounded_corner");
			resetConfigSection("card_holder");
			resetConfigSection("delivery_location");
			resetConfigSection("quantity");
			resetConfigSection("art_option");
			
			if ($(this).is(":checked")) {
				$("#bcard-summary-finishing").html($(this).attr("data-label"));
				$("div.bcard-round-corner-config").hide();
				$("div.bcard-option-radio-wrapper.rounded_corner").parent().removeClass("collapse");
			} else {		
				$("#bcard-summary-finishing").html("-");		
				$("div.bcard-option-radio-wrapper.rounded_corner").parent().addClass("collapse");
			}
			
		} else if (configType == "rounded_corner") {
			
			resetConfigSection("card_holder");
			resetConfigSection("delivery_location");
			resetConfigSection("quantity");
			resetConfigSection("art_option");
			
			if ($(this).is(":checked")) {
				$("#").val($(this).attr("data-label"));				
				//$("div.bcard-option-radio-wrapper.card_holder").parent().removeClass("collapse");
				$("div.bcard-round-corner-config").hide();
				$("div.bcard-round-corner-config input[type=radio]").prop("checked", false);
				
				if ($(this).val() == "one_corner") {
					$("#bcard-round-corner-one_corner").show();
				} else if ($(this).val() == "two_corner") {
					$("#bcard-round-corner-two_corner").show();
				} else if ($(this).val() == "three_corner") {
					$("#bcard-round-corner-three_corner").show();
				} else if ($(this).val() == "four_corner") {
					$("#bcard-round-corner-four_corner").show();
				} else {
					$("#bcard-round-corner-none").show();
				}
			} else {
				$("#").val("-");
				$("div.bcard-option-radio-wrapper.card_holder").parent().addClass("collapse");			
			}
			
		} else if (configType == "card_holder") {
			
			resetConfigSection("delivery_location");
			resetConfigSection("quantity");
			resetConfigSection("art_option");
			if ($(this).is(":checked")) {
				$("#bcard-summary-card-holder").html($(this).attr("data-label"));
				$("div.bcard-option-radio-wrapper.delivery_location").parent().removeClass("collapse");
			} else {
				$("#bcard-summary-card-holder").html("-");
				$("div.bcard-option-radio-wrapper.delivery_location").parent().addClass("collapse");
			}
			
		} else if (configType == "delivery_location") {
			
			resetConfigSection("quantity");
			resetConfigSection("art_option");
			
			if ($(this).is(":checked")) {
				$("#bcard-summary-delivery-location").html($(this).attr("data-label"));
				$("div.bcard-option-radio-wrapper.quantity").parent().removeClass("collapse");
			} else {
				$("#bcard-summary-delivery-location").html("-");
				$("div.bcard-option-radio-wrapper.quantity").parent().addClass("collapse");
			}
			
			/* Time to fetch the quantity and price list */
			 getThePrices();
			
		} else if (configType == "quantity") {
			
			resetConfigSection("art_option");
			
			if ($(this).is(":checked")) {
				$("#bcard-summary-quantity").html($(this).attr("data-label"));
				$("div.bcard-option-radio-wrapper.art_option").parent().removeClass("collapse");
			} else {
				$("#bcard-summary-quantity").html("-");
				$("div.bcard-option-radio-wrapper.art_option").parent().addClass("collapse");
			}
			
		} else if (configType == "art_option") {
			
		}
		
	});
	
	function getThePrices() {
		
		let payload = {};
		
		payload["format"] = $("div.bcard-option-radio-wrapper.format input[type=radio]:checked").val();
		payload["side_of_print"] = $("div.bcard-option-radio-wrapper.side_of_print input[type=radio]:checked").val();
		payload["paper_type"] = $("div.bcard-option-radio-wrapper.paper_type input[type=radio]:checked").val();
		payload["finishing"] = $("div.bcard-option-radio-wrapper.finishing input[type=radio]:checked").val();
		payload["rounded_corner"] = $("div.bcard-option-radio-wrapper.rounded_corner input[type=radio]:checked").val();
		payload["card_holder"] = $("div.bcard-option-radio-wrapper.card_holder input[type=radio]:checked").val();
		//payload["delivery_location"] = $("div.bcard-option-radio-wrapper.delivery_location input[type=radio]:checked").val();
		//payload["quantity"] = $("div.bcard-option-radio-wrapper.quantity input[type=radio]:checked").val();
		//payload["art_option"] = $("div.bcard-option-radio-wrapper.art_option input[type=radio]:checked").val();	
	
		$.ajax({  
			type       : "POST",  
			data       : {action : "bcard_get_pricing_slots", bcard_param : JSON.stringify(payload)},  
			dataType   : "json",  
			url        : bcard_ajax_url,  
			beforeSend : function(){  				
				/* Put your loading mask here */
			},  
			success    : function(data) {				
				renderQuantityPricingSlot(data);		
			},  
			error      : function(jqXHR, textStatus, errorThrown) {                
				alert(jqXHR, textStatus, errorThrown);
			},
			complete   : function() {
				/* Clear the loading mask */
			}   
		});		
		
	}
	
	function renderQuantityPricingSlot(response) {
		
		let html = '';
		let keys = Object.keys(response);
		for (let i = 0; i < keys.length; i++) {
			html += '<label><input type="radio" name="qty-radio" value="'+ keys[i] +'" /> <span class="qty">'+ keys[i] +'</span> <span class="label">RM '+ response[keys[i]].price +'</span><span class="single">'+ response[keys[i]].single +'</span></label>';
		}		
		$("div.bcard-option-radio-wrapper.quantity").html(html);
		
	}
	
	function resetConfigSection(_section) {
		let targetClass = "div.bcard-option-radio-wrapper." + _section;
		/* Reseting the radio buttons */
		$(targetClass +" input").prop("checked", false);
		/* Collapse thge config section */
		$(targetClass).parent().addClass("collapse");
		/* Reset the cart summary property */
		
		if (_section == "format") {
			$("#bcard-summary-format").val("-");
		}
		if (_section == "paper_type") {
			$("#bcard-summary-paper-type").val("-");
		}		
		if (_section == "side_of_print") {
			$("#bcard-summary-pages").val("-");
			$("#bcard-summary-colors").val("-");
		}
		if (_section == "card_holder") {
			$("#bcard-summary-card-holder").val("-");
		}		
		if (_section == "finishing") {
			$("#bcard-summary-finishing").val("-");
		}
		if (_section == "delivery_location") {
			$("#bcard-summary-delivery-location").val("-");	
		}
		if (_section == "quantity") {
			$("#bcard-summary-quantity").val("-");
		}
		
	}		
	
	function addBcardToCart() {
		
		var formData = new FormData();
		formData.append('action', "process_bcard_design");
		
		let uType = $("#bcard-config-user-tab a.active").attr("data-type");		
		if (uType == "details") {
			
			formData.append("user_name", $("#bcard-option-meta-name").val());
			formData.append("user_mobile", $("#bcard-option-meta-mobile").val());
			formData.append("user_email", $("#bcard-option-meta-email").val());
			formData.append("user_company", $("#bcard-option-meta-company").val());
			formData.append("user_designation", $("#bcard-option-meta-designation").val());
						
		} else {	
			let file = $("#bcard-designer-file-input")[0].files[0];   		
			formData.append('bcard_design', file, file.name);			
		}
		
		formData.append("format", $("div.bcard-option-radio-wrapper.format input[type=radio]:checked").val());
		formData.append("side_of_print", $("div.bcard-option-radio-wrapper.side_of_print input[type=radio]:checked").val());
		formData.append("paper_type", $("div.bcard-option-radio-wrapper.paper_type input[type=radio]:checked").val());
		formData.append("finishing", $("div.bcard-option-radio-wrapper.finishing input[type=radio]:checked").val());
		formData.append("rounded_corner", $("div.bcard-option-radio-wrapper.rounded_corner input[type=radio]:checked").val());
		formData.append("card_holder", $("div.bcard-option-radio-wrapper.card_holder input[type=radio]:checked").val());		
		formData.append("qty", $("div.bcard-option-radio-wrapper.quantity input[type=radio]:checked").val());
		
		if (formData) {
		 	$.ajax({
			   	url: bcard_ajax_url,
			   	type: "POST",						
			   	data: formData,
			   	processData: false,
			   	contentType: false,
			   	success: function (res) {
					console.log(res); 
					uploadedUrl = res;
			   	}
			});      		
    	}
		
	}
	
})(jQuery);